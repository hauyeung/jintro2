/**
 	@author Ho Hang Au Yeung
 	@since 2014-04-02
 	game of war
 */

public class PickTwoCards {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		// instantiate Card object
		Card card = new Card();
		
		//pick card for computer
		int num1 = (int)(Math.random()*12+1);
		int suit1 = (int)(Math.random()*4+1);
		
		//set card and suit for computer
		card.setnum(num1);
		card.setsuit(suit1);
		int valcomputer = card.getnum();
		char suitcomputer = card.getsuit();
		
		//output computer's result
		System.out.println(valcomputer+" of "+suitcomputer);
		
		//pick card for player
		int num2 = (int)(Math.random()*12+1);
		int suit2 = (int)(Math.random()*4+1);
		card.setnum(num2);
		card.setsuit(suit2);
		int valplayer = card.getnum();
		char suitplayer = card.getsuit();
		
		//choose suit that is different from computer's
		if (valcomputer==valplayer && suitcomputer==suitplayer)
		{
			int x=0;
			while (x ==suit2 && x<=4)
			{
				card.setsuit(x);
				x++;
			}
		}
		
		//output player's result
		System.out.println(valplayer+" of "+suitplayer);
		if (valcomputer > valplayer)
		{
			System.out.println("Computer wins.");
		}
		else if (valcomputer < valplayer)
		{
			System.out.println("Player wins.");
		}
		else
		{
			System.out.println("Tie.");
		}
		
	}

}

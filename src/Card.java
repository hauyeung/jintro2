
/**
 *  @author Ho Hang Au Yeung
 *  @since 2014-04-02
 *  
 */
public class Card {
	
	char suit = 's';
	int num = 0;
	/**
	 * gets value of card
	 * @return value of card
	 */
	public int getnum()
	{
		return num;	
	}
	/**
	 * set value of card
	 * @param x set value of card, must be integer greater than or equal to 1 or less than or equal to 13
	 */
	public void setnum(int x)
	{
		if (x <1 || x> 13)
		{
			x=1;		
		}
		num = x;
	}
	/**
	 * gets value of suit
	 * @return suit of card
	 */
	public char getsuit()
	{
		return suit;
	}
	/**
	 * set value of suit
	 * @param s integer that determines the suit
	 */
	public void setsuit(int s)
	{
		if (s==1)
		{
			suit = 's';
		}
		else if (s==2)
		{
			suit = 'h';
		}
		else if (s==3)
		{
			suit = 'c';
		}
		else
		{
			suit = 'd';
		}
	}


}
